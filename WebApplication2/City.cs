namespace WebApplication2
{
    public partial class City
    {
        public int ID { get; set; }

        public string CityName { get; set; }

        public string CountryName { get; set; }

        public long Population { get; set; }

        public double? lon { get; set; }

        public double? lat { get; set; }

        public string Code { get; set; }
    }
}
