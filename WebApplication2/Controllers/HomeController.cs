﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string query = @"Select * From City";
            DataTable dataTable = ExecuteQuery(query);
            List<City> cities = new List<City>();
            foreach (DataRow dataRow in dataTable.Rows)
            {
                cities.Add(new City
                {
                    ID = (int)dataRow["ID"],
                    CityName = dataRow["CityName"].ToString(),
                    CountryName = dataRow["CountryName"].ToString(),
                    Population = (long)dataRow["Population"],
                    lon = dataRow["lon"] == DBNull.Value? null : (double?)dataRow["lon"],
                    lat = dataRow["lat"] == DBNull.Value ? null : (double?)dataRow["lat"],
                    Code = dataRow["Code"] == DBNull.Value ? null : dataRow["Code"].ToString()
                });
            }
            return View(cities.Take(10).ToArray());
        }

        private DataTable ExecuteQuery(string query)
        {
            DataTable table = null;
            string connationString  = ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connationString))
            {
                try
                {
                    //SetConnectionArithabortOn(connection);
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = query;
                    //if (sqlParameters != null)
                    //    command.Parameters.AddRange(sqlParameters.GetCopy());
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    if (dataSet.Tables.Count > 0)
                        table = dataSet.Tables[0];
                }
                finally
                {
                    //if (connection.State == ConnectionState.Open)
                    connection.Close();
                }
            }
            return table;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}